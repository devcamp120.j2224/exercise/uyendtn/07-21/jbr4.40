package com.devcamp.s50.invoice_customer.invoicecustomerapi;

import java.util.ArrayList;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.s50.invoice_customer.invoicecustomerapi.model.Customer;
import com.devcamp.s50.invoice_customer.invoicecustomerapi.model.Invoice;

@RestController
public class Controller {
    @CrossOrigin
    @GetMapping("invoices")
    public ArrayList<Invoice> getInvoicesList() {
        //tạo 3 đối tượng khách hàng
        Customer customer1 = new Customer(1,"OHMM",10);
        Customer customer2 = new Customer(2,"XTRA",20);
        Customer customer3 = new Customer(3,"UNIVERSAL",15);
        System.out.println(customer1.toString());
        System.out.println(customer2.toString());
        System.out.println(customer3.toString());

        //tạo 3 đối tượng hoá đơn
        Invoice invoice1 = new Invoice(1, customer1, 10000);
        Invoice invoice2 = new Invoice(2, customer2, 15000);
        Invoice invoice3 = new Invoice(3, customer3, 20000);
        System.out.println(invoice1.toString());
        System.out.println(invoice2.toString());
        System.out.println(invoice3.toString());

        //tạo list invoice, thêm invoice vào list
        ArrayList<Invoice> invoicesList = new ArrayList<Invoice>();
        invoicesList.add(invoice1);
        invoicesList.add(invoice2);
        invoicesList.add(invoice3);
        return invoicesList;
    }
}
