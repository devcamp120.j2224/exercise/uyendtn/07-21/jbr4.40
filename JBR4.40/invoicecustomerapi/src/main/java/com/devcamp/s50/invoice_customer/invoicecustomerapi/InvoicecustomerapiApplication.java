package com.devcamp.s50.invoice_customer.invoicecustomerapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class InvoicecustomerapiApplication {

	public static void main(String[] args) {
		SpringApplication.run(InvoicecustomerapiApplication.class, args);
	}

}
